Build CI container
==================

```
docker build . -t registry.gitlab.developers.cam.ac.uk/ch/co/drupal-9/phpcs-docker:8.3 --build-arg USER_UID=1000 --build-arg USER_GID=1000 --build-arg USER_NAME=user
docker login registry.gitlab.developers.cam.ac.uk -u fjc55
docker push registry.gitlab.developers.cam.ac.uk/ch/co/drupal-9/phpcs-docker:8.3
```

CI templates
============

In your project's .gitlab-ci.yml, you might include a template from here:

```
include:
  - project: 'ch/co/drupal-9/phpcs-docker'
    file: '/templates/.gitlab-ci-phpcs-psr12.yml'

stages:
 ...
 - phpcs
 ...
```

Note that the templates define jobs that run in the phpcs stage, so you may need to explicitly
define that in your stages.

Personal use
============

If you build the container with your own uid etc passed as build args, you can
then mount your home dir as a volume and thus file ownership/permission should
match those on the container host:

```
docker build . -t phpcs:8.3 --build-arg USER_UID=$(id -u) --build-arg USER_GID=$(id -g) --build-arg USER_NAME=$(id -un)
```

You can then run the container with phpcs as an entrypoint:

```
docker run -v /home/alt36/drupal-9:/home/alt36/drupal-9 -u $(id -un) -w $(pwd) -it --rm --entrypoint phpcs phpcs:8.3  $YOUR_PHPCS_ARGS
```

You could define a phpcs() function in .bashrc (note use of $(pwd) in setting the working dir so a simple alias won't work)

```
function phpcs() {
   docker run -v /home/alt36:/home/alt36 -u $(id -un) -w $(pwd) -it --rm --entrypoint phpcs phpcs:8.3 "$@"
}
```

and then run

```
cd ~/drupal-9/ucam_jobs
phpcs --standard=Drupal --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml .
```

To use phpcbf to autofix code, you can specify a different entrypoint:

```
function phpcbf() {
  docker run -v /home/alt36:/home/alt36 -u $(id -un) -w $(pwd) -it --rm --entrypoint phpcbf phpcs:8.3 --standard=Drupal "$@"
}
```
