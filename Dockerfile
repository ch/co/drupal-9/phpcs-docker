FROM ubuntu:22.04

ARG USER_NAME
ARG USER_UID
ARG USER_GID

RUN groupadd --gid $USER_GID $USER_NAME
RUN useradd --uid $USER_UID --gid $USER_GID $USER_NAME

ENV COMPOSER_HOME=/usr/local

RUN ln -fs /usr/share/zoneinfo/Europe/London /etc/localtime

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
  git \
  unzip \
  php-cli \
  php-mbstring \
  php-xml \
  curl

RUN curl https://getcomposer.org/download/2.7.2/composer.phar -o /usr/local/bin/composer
RUN chmod +x /usr/local/bin/composer

RUN composer global config allow-plugins true
RUN composer global require drupal/coder

ENV PATH="${PATH}:${COMPOSER_HOME}/vendor/bin"
